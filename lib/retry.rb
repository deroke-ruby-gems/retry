def retryNTimes(n = 3, &block)
  callback = block
  i = 0

  begin
    callback.call()
  rescue
    puts "Error: #{$!}".red

    i += 1
    suffix = i == n ? "aborting" : "trying again"
    puts "Command failed on try #{i} of #{n}, #{suffix} ...".yellow

    retry if i < n

    exit(1)
  end
end

# https://stackoverflow.com/a/11482430
class String
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def yellow
    colorize(33)
  end

  def blue
    colorize(34)
  end

  def pink
    colorize(35)
  end

  def light_blue
    colorize(36)
  end
end
