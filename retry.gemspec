Gem::Specification.new do |s|
  s.name        = 'retry'
  s.version     = '0.0.1'
  s.summary     = "Retry blocks of code"
  s.description = "Retry n times blocks of code"
  s.authors     = ["deroke"]
  s.files       = ["lib/retry.rb"]
  s.homepage    = 'https://gitlab.com/deroke-ruby-gems/retry/'
  s.license     = 'MIT'
end
